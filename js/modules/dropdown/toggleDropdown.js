import { fadeIn, fadeOut } from '../functions.js'
import formValidate from '../validation/instance.js'

const dropdownItems = document.querySelectorAll('.dropdown')

export const dropdown = () => {
  dropdownItems.forEach(item => {
    item.querySelector('.dropdown__current').addEventListener('click', e => {
      e.stopPropagation()

      const activeEl = document.querySelector('.dropdown.open')
      activeEl && activeEl.classList.remove('open')
      
      item.classList.toggle('open')
    })
  })
}

export const setActiveElem = () => {
  dropdownItems.forEach(item => {
    const activeContainer = item.querySelector('.dropdown__text')
    const input = item.querySelector('input[type="hidden"]')
        
    item.querySelectorAll('.dropdown__item').forEach(innerItem => {
      innerItem.addEventListener('click', () => {
        const activeEl = item.querySelector('.dropdown__item.active')
        
        const paymentInp = document.querySelector('#payment-method')
        const deliveryInp = document.querySelector('#delivery-type')
        
        const paymentField = document.querySelector('[data-field="code"]')
        const warehouseField = document.querySelector('[data-field="warehouse"]')
        const addressField = document.querySelector('[data-field="address"]')

        const checkoutForm = document.querySelector('.checkout__form')
        
        activeEl && activeEl.classList.remove('active')
        innerItem.classList.add('active')
        activeContainer.textContent = innerItem.textContent

        input.value = innerItem.dataset.value
        input.classList.remove('just-validate-error-field')

        item.classList.remove('open')

        if (!paymentInp) return 

        if (paymentInp.value === 'score') {
          fadeIn(paymentField, 250, 'block')

          // formValidate(checkoutForm)
          //   .addField('#checkout-code', [
          //     {
          //       rule: 'required',
          //       value: true,
          //       errorMessage: "Code is required",
          //     },
          //   ])
        } else {
          fadeOut(paymentField, 250)

          // formValidate(checkoutForm).removeField('#checkout-code')
        }

        if (deliveryInp.value === 'warehouse') {
          fadeOut(addressField, 250)

          setTimeout(() => fadeIn(warehouseField, 250, 'block'), 250)
        }

        if (deliveryInp.value === 'courier') {
          fadeOut(warehouseField, 250)

          setTimeout(() => fadeIn(addressField, 250, 'block'), 250)
        }
      })
    })
  })
}

export const clickOutside = () => {
  window.addEventListener('click', e => {
    e.stopPropagation()
    
    if ([...dropdownItems].some(dropdown => dropdown.classList.contains('open')) && !e.target.closest('.dropdown')) {
      dropdownItems.forEach(dropdown => dropdown.classList.remove('open'))
    }
  })
}

if (dropdownItems.length > 0) {
  dropdown()
  setActiveElem()
  clickOutside()
}