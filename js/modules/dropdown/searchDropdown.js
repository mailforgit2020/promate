const searchDropdownInps = document.querySelectorAll('.dropdown__search')

export const searchDropdown = () => {
  searchDropdownInps.forEach(inp => {
    inp.addEventListener('input', () => {
      const items = [...inp.parentElement.querySelectorAll('.dropdown__item')]
      let value = inp.value.trim().toLowerCase()

      items.forEach(item => {
        let text = item.textContent.trim().toLowerCase()

        text.indexOf(value) !== -1 ? item.style.display = 'block' : item.style.display = 'none'
      })


      if (items.every(item => item.style.display === 'none')) {
        inp.nextElementSibling.style.display = 'none'
        inp.nextElementSibling.nextElementSibling.style.display = 'block'
      } else {
        inp.nextElementSibling.style.display = 'block'
        inp.nextElementSibling.nextElementSibling.style.display = 'none'
      }
    })
  })
}

searchDropdownInps.length > 0 && searchDropdown()