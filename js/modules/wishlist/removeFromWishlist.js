const removeBtn = document.querySelectorAll('.account-content__clear')
const errTitle = document.querySelector('.account-content__err')

export const removeFromWishlist = () => {
  removeBtn.forEach(btn => {
    btn.addEventListener('click', () => {
      const productItems = [...document.querySelectorAll('.product-item')]
  
      productItems.forEach(item => item.remove())
      productItems.length = 0
  
      if (!productItems.length) {
        errTitle.classList.add('show')

        removeBtn.forEach(btn => btn.disabled = true)
      }
    })  
  })
  
}

removeBtn.length > 0 && removeFromWishlist()