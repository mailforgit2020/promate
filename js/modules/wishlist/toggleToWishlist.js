const likesBtns = document.querySelectorAll('.product-item__like, .product-info__action-btn')

export const toggleToWishlist = () => {
  likesBtns.forEach(btn => {
    btn.addEventListener('click', () => {
      btn.classList.toggle('liked')
    })
  })
}

likesBtns.length > 0 && toggleToWishlist()