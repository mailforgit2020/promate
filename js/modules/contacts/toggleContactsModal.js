import { fadeToggle, enableScrollbar, disableScrollbar } from '../functions.js'

const openContactsBtn = document.querySelector('.footer-lists__btn')
const closeContactsBtn = document.querySelector('.contacts-modal__times')
const contactsModal = document.querySelector('.contacts-modal')
const overlay = document.querySelector('.overlay')

export const openContactsModal = () => {
  openContactsBtn.addEventListener('click', () => {
    openContactsBtn.classList.add('active')
    contactsModal.classList.add('open')

    fadeToggle(overlay, 200, 'block')

    disableScrollbar()
  })
}

export const closeContactsModal = () => {
  closeContactsBtn.addEventListener('click', () => {
    openContactsBtn.classList.remove('active')
    contactsModal.classList.remove('open')
  
    fadeToggle(overlay, 350, 'block')
  
    enableScrollbar()
  })

  overlay.addEventListener('click', () => closeContactsBtn.click())
}

if (contactsModal) {
  openContactsModal()
  closeContactsModal()
}