const couponBtns = [...document.querySelectorAll('.coupon-btn')]

export const showCoupon = () => {
  couponBtns.forEach(btn => {
    btn.addEventListener('click', e => {
      e.stopPropagation()
      
      btn.classList.toggle('active')
      btn.nextElementSibling.classList.toggle('active')
    })
  })
}

export const closeCoupon = () => {
  window.addEventListener('click', e => {
    if (couponBtns.some(btn => btn.classList.contains('active')) && !e.target.closest('.coupon-inp')) {
      couponBtns.forEach(btn => {
        btn.classList.remove('active')
        btn.nextElementSibling.classList.remove('active')
      })
    }
  })
}

couponBtns.length > 0 && showCoupon()
couponBtns.length > 0 && closeCoupon()