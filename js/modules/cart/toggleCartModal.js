const openCartBtn = document.querySelector('.header-controls__btn')

export const openCartModal = () => {
  openCartBtn.addEventListener('mouseenter', () => {
    if (innerWidth <= 1024) return
    const cartModal = document.querySelector('.modal-cart')
    
    openCartBtn.classList.add('active')
    cartModal.classList.add('open')
  })
}

export const closeCartModal = () => {
  document.addEventListener('click', e => {
    if (e.target.closest('.modal-cart__times')) {
      const cartModal = document.querySelector('.modal-cart')
      
      openCartBtn.classList.remove('active')
      cartModal.classList.remove('open')
    }
  })

  document.addEventListener('click', e => {
    const cartModal = document.querySelector('.modal-cart')
    
    if (cartModal && cartModal.classList.contains('open') && !e.target.closest('.modal-cart')) {
      document.querySelector('.modal-cart__times').click()
    }
  })
}

openCartBtn && openCartModal()
openCartBtn && closeCartModal()