export const incrementCount = () => {
  document.addEventListener('click', e => {
    if (e.target.closest('.count-plus')) {
      const btn = e.target.closest('.count-plus')
      const input = btn.previousElementSibling

      if (input.value >= 9999) return

      input.value = parseInt(input.value) + 1
    }
  })
}

incrementCount()