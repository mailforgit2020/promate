import '../../vendor/simplebar.min.js'

const appendCart = () => {
  const header = document.querySelector('.header .header__inner')
  const cartModalTemplate = document.querySelector('#modal-cart')
  let cartModal = document.querySelector('.modal-cart')

  if (innerWidth > 1024) {
    if (!cartModal && cartModalTemplate) {
      const cartModalClone = document.importNode(cartModalTemplate.content, true)
      
      cartModal = cartModalClone.firstElementChild
      
      header.insertAdjacentElement('beforeend', cartModal)

      const list = document.querySelector('.modal-cart__products')
      const listInstance = new SimpleBar(list, { autoHide: false, })
      listInstance.recalculate()
    } else if (cartModal && cartModal.contains(cartModalTemplate)) {
      cartModal.remove()
    }
  } else {
    if (cartModal) {
      cartModal.remove()
      cartModal = null
    }
  }
}

window.addEventListener('DOMContentLoaded', appendCart)
window.addEventListener('resize', appendCart)