export const displayCount = () => {
  document.addEventListener('input', e => {
    if (e.target.closest('.count-display')) {
      const inp = e.target.closest('.count-display')

      let value = inp.value

      if (value.match(/[^0-9]/g)) inp.value = value.replace(/[^0-9]/g, '')
      else inp.value = value

      if (value.trim() === '') inp.value = 1

      if (parseInt(value) <= 1) inp.value = 1

      if (parseInt(value) >= 9999) inp.value = 9999
    }
  })
}

displayCount()