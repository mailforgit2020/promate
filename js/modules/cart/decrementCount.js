export const decrementCount = () => {
  document.addEventListener('click', e => {
    if (e.target.closest('.count-minus')) {
      const btn = e.target.closest('.count-minus')

      const input = btn.nextElementSibling

      if (input.value <= 1) return

      input.value = parseInt(input.value) - 1
    }
  })
}

decrementCount()