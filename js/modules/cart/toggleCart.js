import { fadeToggle, enableScrollbar, disableScrollbar } from '../functions.js'

const cart = document.querySelector('.cart')
const openCartBtn = document.querySelector('.header-controls__btn')
const closeCartBtn = document.querySelector('.cart__times')
const overlay = document.querySelector('.overlay')

export const openCart = () => {
  openCartBtn.addEventListener('click', () => {
    const cartModal = document.querySelector('.modal-cart')
    
    openCartBtn.classList.add('active')
    cart.classList.add('open')
    cartModal && cartModal.classList.remove('open')

    fadeToggle(overlay, 200, 'block')

    disableScrollbar()
  })
}

export const closeCart = () => {
  closeCartBtn.addEventListener('click', () => {
    openCartBtn.classList.remove('active')
    cart.classList.remove('open')

    fadeToggle(overlay, 350, 'block')

    enableScrollbar()
  })

  overlay.addEventListener('click', () => closeCartBtn.click())
}

cart && openCart()
cart && closeCart()
