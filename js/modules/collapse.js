const collapseBtns = document.querySelectorAll('.collapse-btn')

export const collapseBlock = () => {
  collapseBtns.forEach(btn => {
    btn.addEventListener('click', () => {
      const block = btn.parentElement.previousElementSibling
      const blockHeight = block.scrollHeight

      block.style.height = !block.classList.contains('open') ? `${blockHeight}px` : ''
      btn.textContent = !block.classList.contains('open') ? 'Скрыть' : 'Читать дальше' 

      block.classList.toggle('open')
    })
  })
}

collapseBtns.length > 0 && collapseBlock();