// import { tabsSlider } from './sliders.js'

// const tavNavItems = document.querySelectorAll('.tabs-head__item')

const tabsSlider = document.querySelectorAll('.tabs')

export const tabs = () => {
  tabsSlider.forEach((slider, sliderIndex) => {

    const swiper = slider.querySelector('.tabs-body').swiper

    slider.querySelectorAll('.tabs-head__item').forEach((item, itemIndex) => {
      item.addEventListener('click', () => {
        slider.querySelector('.tabs-head__item.active').classList.remove('active')
        item.classList.add('active')
        swiper.slideTo(itemIndex)
      })
    })
  })
  
  // tavNavItems.forEach((item, index) => {
  //   item.addEventListener('click', () => {
  //     document.querySelector('.tabs-head__item.active').classList.remove('active')
  //     item.classList.add('active')

  //     const slider = item.parentElement.nextElementSibling.swiper

  //     console.log(slider)
      
  //     slider.slideTo(index)
  //   })
  // })
}

tabsSlider.length > 0 && tabs()