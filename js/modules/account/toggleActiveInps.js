const editInps = document.querySelectorAll('[disabled]')
const editBtn = document.querySelector('.account-content__edit')

export const toggleActiveInps = () => {
  editBtn.addEventListener('click', () => {
    editBtn.classList.toggle('active')
    editInps.forEach(inp => {
      inp.disabled = editBtn.classList.contains('active') ? false : true
    })
  })
}

editBtn && toggleActiveInps()