import { fadeToggle, enableScrollbar, disableScrollbar } from '../functions.js'

const sidebar = document.querySelector('.account-sidebar')
const openSidebarBtn = document.querySelector('.account__btn')
const closeSidebarBtn = document.querySelector('.account-sidebar__times')
const overlay = document.querySelector('.overlay')

export const openSidebar = () => {
  openSidebarBtn.addEventListener('click', () => {
    openSidebarBtn.classList.add('active')
    sidebar.classList.add('open')

    fadeToggle(overlay, 200, 'block')

    disableScrollbar()
  })
}

export const closeSidebar = () => {
  closeSidebarBtn.addEventListener('click', () => {
    openSidebarBtn.classList.remove('active')
    sidebar.classList.remove('open')

    fadeToggle(overlay, 350, 'block')

    enableScrollbar()
  })

  overlay.addEventListener('click', () => closeSidebarBtn.click())
}

sidebar && openSidebar()
sidebar && closeSidebar()
