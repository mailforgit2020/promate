const zoomHandler = e => {
  if (innerWidth <= 1024) return
  
  const zoomer = e.currentTarget
  const offsetX = e.offsetX || (e.touches[0].pageX - zoomer.getBoundingClientRect().left)
  const offsetY = e.offsetY || (e.touches[0].pageY - zoomer.getBoundingClientRect().top)

  const zoomScale = 1.5

  const x = offsetX / zoomer.offsetWidth * 100
  const y = offsetY / zoomer.offsetHeight * 100

  zoomer.style.backgroundSize = `${zoomScale * 100}%`
  zoomer.style.backgroundPosition = `${x}% ${y}%`
}

const images = document.querySelectorAll('.zoom-img')

export const zoomImage = () => images.forEach(img => img.addEventListener('mousemove', zoomHandler))

images && zoomImage()