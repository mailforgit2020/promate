import { anchorScroll } from './functions.js'

const btn = document.querySelector('.btn-up')

const toggleVisibleBtn = () => {
  scrollY > 200 ? btn.classList.add('visible') : btn.classList.remove('visible')
}

export const setVisibleBtn = () => {
  window.addEventListener('scroll', toggleVisibleBtn, { passive: false, })
  window.addEventListener('load', toggleVisibleBtn)
  window.addEventListener('resize', toggleVisibleBtn)
}

export const scrollUp = () => {
  btn.addEventListener('click', () => {
    anchorScroll(window, 0, 1, 0)
  })
}


btn && setVisibleBtn()
btn && scrollUp()