const cookiePannel = document.querySelector('.cookie-panel')
const cookieBtn = cookiePannel.querySelector('.cookie-panel__btn')

export const cookieCondense = () => {
  cookieBtn.addEventListener('click', () => {
    cookiePannel.classList.add('hidden')
  })
}

cookiePannel && cookieCondense()