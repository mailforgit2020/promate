import { GOOGLE_MAPS_API_KEY } from '../vars.js'
import GoogleMapsApi from './GoogleMapsApi.js'
import { styles } from './styles.js'

const initMap = mapSelector => {
  const element = document.getElementById(mapSelector);
  const map = new google.maps.Map(element, {
    zoom: 15,
    center: {
      lat: 50.2630256,
      lng: 28.6594693
    },
    styles: styles
  });

  const marker = new google.maps.Marker({
    position: {
      lat: 50.2630256,
      lng: 28.6594693
    },
    label: { 
      color: '#fff', 
      fontWeight: '500', 
      fontSize: '10px', 
      lineHeight: '150%', 
      className: 'map-marker', 
      text: ' ' 
    },
    optimized: true,
    map: map,
    icon: '../img/map-marker.svg',
  })
}

const gmapApi = new GoogleMapsApi(GOOGLE_MAPS_API_KEY)
gmapApi.load().then(() => {
  const map = document.querySelector('#map')
  map && initMap('map')
})