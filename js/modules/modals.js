import { getScrollbarWidth } from './functions.js'

const closeModal = document.querySelectorAll('.close-modal')
const fixedBlocks = document.querySelectorAll('.fixed-block')
const btnUp = document.querySelector('.btn-up')
const btnUpOffset = btnUp && window.getComputedStyle(btnUp)

const mainSliderCurrent = document.querySelector('.modal-product__current')

Fancybox.bind('[data-fancybox]', {
	dragToClose: false,
	showClass: 'fancybox-fadeIn',
	hideClass: 'fancybox-fadeOut',
	on: {
		init: () => {
			// fixedBlocks.forEach(item => item.style.paddingRight = `${getScrollbarWidth()}px`)
			// btnUp.style.right = `${getScrollbarWidth() + parseInt(btnUpOffset.getPropertyValue('right'))}px`
		},
		ready: () => {
			const popupSliders = document.querySelectorAll('.popup-sliders')

			popupSliders.forEach(slider => {
				const tavNavItems = slider.querySelectorAll('.popup-thumb__item')

				new Swiper(slider.querySelector('.popup-main-slider'), {
					effect: 'fade',
					autoHeight: true,
					speed: 200,
					allowTouchMove: false,
					fadeEffect: {
						crossFade: false
					},
					navigation: {
						prevEl: slider.parentElement.querySelector('.popup-main__btn--prev'),
						nextEl: slider.parentElement.querySelector('.popup-main__btn--next'),
					},
					pagination: {
						el: '.modal-product__total',
						type: 'custom',
						renderCustom: (swiper, current, total) => {
							return `${total}`
						}
					},
					on: {
						init: swiper => {
							const productInfo = swiper.el.parentElement
							const productThumb = productInfo.previousElementSibling
							
							setTimeout(() => {
								if (productThumb && innerWidth > 1025) {
									let productInfoHeight = productInfo.offsetHeight
							
									productThumb.style.height = `${productInfoHeight}px`
								}
							}, 100)
						},
						slideChange: swiper => {
							let index = swiper.realIndex
	
							slider.querySelector('.popup-thumb__item.active').classList.remove('active')
							tavNavItems[index].classList.add('active')

							mainSliderCurrent.innerHTML = mainSliderCurrent && index + 1
						}
					}
				})
			})

			popupSliders.forEach(slider => {
				slider.querySelectorAll('.popup-thumb__item').forEach((item, index) => {
					item.addEventListener('click', () => {
						const swiper = slider.querySelector('.popup-main-slider').swiper
						
						slider.querySelector('.popup-thumb__item.active').classList.remove('active')
						item.classList.add('active')

						swiper.slideTo(index)
					})
				})
			})
		},
		destroy: () => {
			// fixedBlocks.forEach(item => item.style.paddingRight = '0')
			// btnUp.style.right = ''
		}
	}
})

closeModal.forEach(btn => {
	btn.addEventListener('click', () => {
		Fancybox.close()
	})
})