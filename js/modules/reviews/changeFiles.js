import { loadImages } from './loadImages.js'

const fileInps = document.querySelectorAll('.product-tabs__review-files-inp')

export const changeFiles = () => {
  fileInps.forEach(inp => {
    inp.addEventListener('change', () => {
      const imgContainer = inp.previousElementSibling
      const mockContainer = inp.previousElementSibling.querySelector('.product-tabs__review-files-visible')

      loadImages(inp.files, imgContainer, mockContainer, 10, imgContainer.querySelectorAll('.product-tabs__review-files-photo'))
    })
  })
}

fileInps.length > 0 && changeFiles()