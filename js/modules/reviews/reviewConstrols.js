const reviewColntolBtn = document.querySelectorAll('.like-btn')

export const reviewControls = () => {
  reviewColntolBtn.forEach(btn => {
    btn.addEventListener('click', () => {
      let count = btn.querySelector('span')
      
      btn.classList.toggle('clicked')
      btn.classList.contains('clicked') ? count.textContent = parseInt(count.textContent) + 1 : count.textContent = parseInt(count.textContent) - 1 
  
      if (btn.classList.contains('like-btn--plus')) {
        let dislikeBtn = btn.nextElementSibling,
            dislikeCount = dislikeBtn.querySelector('span')
  
        dislikeBtn.classList.contains('clicked') ? dislikeCount.textContent = parseInt(dislikeCount.textContent) - 1 : null
        dislikeBtn.classList.remove('clicked')
  
      } else {
        let likeBtn = btn.previousElementSibling
        let likeCount = likeBtn.querySelector('span')
  
        likeBtn.classList.contains('clicked') ? likeCount.textContent = parseInt(likeCount.textContent) - 1 : null
        likeBtn.classList.remove('clicked')
      }
    })
  })
}

reviewColntolBtn.length > 0 && reviewControls()