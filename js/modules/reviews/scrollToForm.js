import { anchorScroll } from '../functions.js'

const scrollBtns = document.querySelectorAll('.product-tabs__review-average-btn')
const headerHeight = document.querySelector('.header').offsetHeight

export const scrollToForm = () => {
  scrollBtns.forEach(btn => {
    btn.addEventListener('click', () => {
      const anchorBlockId = `#${btn.parentElement.parentElement.parentElement.nextElementSibling.id}`

      anchorScroll(window, anchorBlockId, 1, headerHeight)
    })
  })
}

scrollBtns.length > 0 && scrollToForm()