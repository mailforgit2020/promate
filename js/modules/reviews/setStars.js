const stars = document.querySelectorAll('[data-rating-item]')

export const setStars = () => {
  stars.forEach(star => {
    star.addEventListener('click', () => {
      const parent = star.parentElement

      parent.dataset.rating = star.dataset.ratingItem
    })
  })
}

stars.length > 0 && setStars()