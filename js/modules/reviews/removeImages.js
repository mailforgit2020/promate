export const removeImages = () => {
  document.addEventListener('click', e => {
    if (e.target.closest('.product-tabs__review-files-times')) {
      const btn = e.target.closest('.product-tabs__review-files-times')
      const imgContainer = btn.parentElement.parentElement 
      const visibleContainer = imgContainer.querySelector('.product-tabs__review-files-visible')
      const photos = imgContainer.querySelectorAll('.product-tabs__review-files-photo')
      const input = imgContainer.nextElementSibling

      const index = Array.from(photos).indexOf(btn.parentElement)
      
      btn.parentElement.remove()
      removeFileFromInput(input, index)

      visibleContainer.style.display = photos.length > 1 ? 'none' : 'flex'
    }
  })
}

function removeFileFromInput(input, index) {
  const files = Array.from(input.files)
  files.splice(index, 1)
  const newFileList = new DataTransfer()
  files.forEach(file => newFileList.items.add(file))
  input.files = newFileList.files
}

removeImages()