export const loadImages = (files, imgContainer, mockContainer, maxLength, photoLength) => {
  if (files.length > maxLength - 1 || photoLength.length > maxLength - 1) return

  console.log(photoLength.length)
    
  for (var i = 0; i < files.length; i++) {
    const reader = new FileReader()
    
    reader.addEventListener('load', e => {
      let imgBlock = `
        <div class="product-tabs__review-files-photo icon">
          <span class="product-tabs__review-files-times"></span>
          <img src="${e.target.result}" width="80" height="70" alt="" />
        </div>
      `
    
      mockContainer.style.display = 'none'
      imgContainer.insertAdjacentHTML('beforeend', imgBlock)
    })

    reader.readAsDataURL(files[i])
  }
}