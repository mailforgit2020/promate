import { loadImages } from './loadImages.js'

const dropZone = document.querySelector('.product-tabs__content .product-tabs__review-files-visible')

export const dragFiles = () => {
  dropZone.addEventListener('dragover', e => {
    e.preventDefault()
  })

  dropZone.addEventListener('dragenter', () => {
    dropZone.classList.add('drag-over')
  })

  dropZone.addEventListener('dragleave', () => {
    dropZone.classList.remove('drag-over')
  })

  dropZone.addEventListener('drop', e => {
    e.preventDefault()
    dropZone.classList.remove('drag-over')
  
    const files = e.dataTransfer.files
    const imgContainer = dropZone.parentElement

    loadImages(files, imgContainer, dropZone, 10, imgContainer.querySelectorAll('.product-tabs__review-files-photo'))
  })
}

dropZone && dragFiles()