import { slideToggle } from './functions.js'

const accordionItems = document.querySelectorAll('.accordion__head, .footer-title')
const catalogBtn = document.querySelector('.catalog-filters__header-side')
const commentBtn = document.querySelector('.checkout-contacts__textarea-times')

export const accordion = () => {
  accordionItems.forEach(head => {
    head.addEventListener('click', () => {
      slideToggle(head.nextElementSibling, 250, 'block')
      head.parentElement.classList.toggle('active')
    })
  })
}

export const catalogCollapse = () => {
  catalogBtn.addEventListener('click', () => {
    const parent = catalogBtn.parentElement.parentElement
    const body = parent.querySelector('.catalog-filters__list')

    slideToggle(body, 250, 'block')
    parent.classList.toggle('active')
  })
}

export const commentClose = () => {
  commentBtn.addEventListener('click', () => {
    const parent = commentBtn.parentElement.parentElement.parentElement.parentElement
    const body = parent.querySelector('.checkout-contacts__comment-body')

    slideToggle(body, 250, 'block')
    parent.classList.toggle('active')
  })
}

accordionItems.length > 0 && accordion()
catalogBtn && catalogCollapse()
commentBtn && commentClose()