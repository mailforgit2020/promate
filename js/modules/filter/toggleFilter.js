import { disableScrollbar, enableScrollbar, fadeToggle } from '../functions.js'

const filterOpenBtn = document.querySelector('.catalog__filter')
const filterCloseBtn = document.querySelector('.catalog-sidebar__head-btn')
const filterSection = document.querySelector('.catalog-sidebar')
const overlay = document.querySelector('.overlay')

export const openFilter = () => {
  filterOpenBtn.addEventListener('click', () => {
    filterOpenBtn.classList.add('active')
    filterSection.classList.add('open')

    fadeToggle(overlay, 200, 'block')

    disableScrollbar()
  })
}

export const closeFilter = () => {
  filterCloseBtn.addEventListener('click', () => {
    filterOpenBtn.classList.remove('active')
    filterSection.classList.remove('open')

    fadeToggle(overlay, 350, 'block')

    enableScrollbar()
  })

  overlay.addEventListener('click', () => filterCloseBtn.click())
}

if (filterSection) {
  openFilter()
  closeFilter()
}