const resetFilterBtns = document.querySelectorAll('.reset-filter')

export const resetFilter = () => {
  resetFilterBtns.forEach(btn => {
    btn.addEventListener('click', () => {
      const filterItems = [...document.querySelectorAll('.catalog-selected__item')]
      const filterContainers = [...document.querySelectorAll('.catalog-selected')]

      filterItems.forEach(item => item.remove())
      filterContainers.forEach(container => container.style.display = 'none')
    })
  })
}

resetFilterBtns.length > 0 && resetFilter()