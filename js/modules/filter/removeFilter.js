export const removeFilter = () => {
  document.addEventListener('click', e => {
    if (e.target.closest('.catalog-selected__item-icon')) {
      const target = e.target.closest('.catalog-selected__item-icon')
      const targetText = target.parentElement.querySelector('.catalog-selected__item-text').textContent.trim().toLowerCase()
      const filterItems = [...document.querySelectorAll('.catalog-selected__item')]
      const filterContainers = [...document.querySelectorAll('.catalog-selected')]

      filterItems.forEach(item => item.querySelector('.catalog-selected__item-text').textContent.trim().toLowerCase() === targetText && item.remove())
      filterContainers.forEach(container => container.style.display = filterItems.length / 2 > 1 ? 'flex' : 'none')
    }
  })
}

removeFilter()