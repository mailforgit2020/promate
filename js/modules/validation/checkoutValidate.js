import formValidate from './instance.js'

const checkoutForm = document.querySelector('.checkout__form')

export const checkoutValidate = () => {
  checkoutForm.querySelectorAll('.checkout-contacts__inp').forEach(inp => {
    inp.addEventListener('change', () => {
      const hiddenInps = document.querySelectorAll('input[type="hidden"]')

      hiddenInps.forEach(hiddenInp => {
        if (hiddenInp.type !== 'hidden' && hiddenInp.value !== '') {
          const siblingInput = hiddenInp.nextElementSibling;
          if (siblingInput && siblingInput.classList.contains('just-validate-error-field')) {
            siblingInput.classList.remove('just-validate-error-field');
            siblingInput.classList.add('just-validate-success-field');
          }
        }
      })
    })
  })
  
  formValidate(checkoutForm)
    .addField('#checkout-name', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Name is required",
      },
      {
        rule: 'minLength',
        value: 3,
        errorMessage: "To short value"
      },
      {
        rule: 'maxLength',
        value: 10,
        errorMessage: "To long value"
      },
    ])
    .addField('#checkout-lastname', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Name is required",
      },
      {
        rule: 'minLength',
        value: 3,
        errorMessage: "To short value"
      },
      {
        rule: 'maxLength',
        value: 20,
        errorMessage: "To long value"
      },
    ])
    .addField('#checkout-phone', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Phone is required",
      },
      {
        rule: 'customRegexp',
        value: /^\+38\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$/gi,
        errorMessage: 'Phone is required',
      },
    ])
    .addField('#checkout-connection', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Phone is required",
      },
    ])
    .addField('#delivery-method', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Phone is required",
      },
    ])
    .addField('#delivery-type', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Phone is required",
      },
    ])
    .addField('#delivery-city', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Phone is required",
      },
    ])
    .addField('#delivery-warehouse', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Phone is required",
      },
    ])
    .addField('#payment-method', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Phone is required",
      },
    ])
    .onSuccess((event) => {
      console.log('Validation passes and form submitted', event)
    })
    .onFail((fields) => {
      console.log('Form failds', fields)
    })
}

checkoutForm && checkoutValidate()