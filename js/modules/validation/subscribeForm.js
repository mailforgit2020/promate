import formValidate from './instance.js'

const subscribeForm = document.querySelector('.footer-form')

export const subscribeValidate = () => {
  formValidate(subscribeForm)
    .addField('#footer-email', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Email is required",
      },
      {
        rule: 'email',
        value: true,
        errorMessage: "Email is invalid",
      },
    ])
    .onSuccess((event) => {
      console.log('Validation passes and form submitted', event);
    })
    .onFail((fields) => {
      console.log('Form failds', fields);
    })
}

subscribeForm && subscribeValidate()