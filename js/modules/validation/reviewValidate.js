import formValidate from './instance.js'

const reviewForms = document.querySelectorAll('.product-tabs__review-form')

export const reviewValidate = () => {
  reviewForms.forEach(form => {
    formValidate(form)
      .addField('[name="advantages"]', [
        {
          rule: 'required',
          value: true,
          errorMessage: "Advantages is required",
        },
      ])
      .addField('[name="flaws"]', [
        {
          rule: 'required',
          value: true,
          errorMessage: "Flaws is required",
        },
      ])
      .addField('[name="comment"]', [
        {
          rule: 'required',
          value: true,
          errorMessage: "Flaws is required",
        },
      ])
      .onSuccess((event) => {
        console.log('Validation passes and form submitted', event)
      })
      .onFail((fields) => {
        console.log('Form failds', fields)
      })
  })
}

reviewForms.length > 0 && reviewValidate()