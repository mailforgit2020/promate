import formValidate from './instance.js'

const checkoutAuthForm = document.querySelector('.checkout-login__form')

export const checkoutAuthValidate = () => {
  formValidate(checkoutAuthForm)
    .addField('#checkout-login-phone', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Phone is required",
      },
      {
        rule: 'customRegexp',
        value: /^\+38\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$/gi,
        errorMessage: 'Phone is required',
      },
    ])
    .onSuccess((event) => {
      console.log('Validation passes and form submitted', event);
    })
    .onFail((fields) => {
      console.log('Form failds', fields);
    })
}

checkoutAuthForm && checkoutAuthValidate()