import formValidate from './instance.js'

const stockForm = document.querySelector('.modal-stock__form')

export const stockValidate = () => {
  formValidate(stockForm)
    .addField('#stock-email', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Email is required",
      },
      {
        rule: 'email',
        value: true,
        errorMessage: "Email is invalid",
      },
    ])
    .onSuccess((event) => {
      console.log('Validation passes and form submitted', event);
    })
    .onFail((fields) => {
      console.log('Form failds', fields);
    })
}

stockForm && stockValidate()