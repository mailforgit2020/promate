import formValidate from './instance.js'

const constactsForm = document.querySelector('.contacts-callback__form')

export const constactsValidate = () => {
  formValidate(constactsForm)
    .addField('#contacts-name', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Name is required",
      },
      {
        rule: 'minLength',
        value: 3,
        errorMessage: "To short value"
      },
      {
        rule: 'maxLength',
        value: 10,
        errorMessage: "To long value"
      },
    ])
    .addField('#contacts-phone', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Phone is required",
      },
      {
        rule: 'customRegexp',
        value: /^\+38\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$/gi,
        errorMessage: 'Phone is required',
      },
    ])
    .addField('#contacts-email', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Email is required",
      },
      {
        rule: 'email',
        value: true,
        errorMessage: "Email is invalid",
      },
    ])
    .onSuccess((event) => {
      console.log('Validation passes and form submitted', event);
    })
    .onFail((fields) => {
      console.log('Form failds', fields);
    })
}

constactsForm && constactsValidate()