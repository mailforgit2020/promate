import formValidate from './instance.js'

const troubleForm = document.querySelector('.modal-trouble__form')

export const troubleValidate = () => {
  formValidate(troubleForm)
    .addField('#trouble-phone', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Phone is required",
      },
    ])
    .addField('#trouble-message', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Message is required",
      },
    ])
    .onSuccess((event) => {
      console.log('Validation passes and form submitted', event);
    })
    .onFail((fields) => {
      console.log('Form failds', fields);
    })
}

troubleForm && troubleValidate()