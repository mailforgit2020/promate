import formValidate from './instance.js'

const accountForm = document.querySelector('.account-content__form')

export const accountValidate = () => {
  formValidate(accountForm)
    .addField('#account-name', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Name is required",
      },
      {
        rule: 'minLength',
        value: 3,
        errorMessage: "To short value"
      },
      {
        rule: 'maxLength',
        value: 10,
        errorMessage: "To long value"
      },
    ])
    .addField('#account-lastname', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Last name is required",
      },
      {
        rule: 'minLength',
        value: 3,
        errorMessage: "To short value"
      },
      {
        rule: 'maxLength',
        value: 10,
        errorMessage: "To long value"
      },
    ])
    .addField('#account-phone', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Phone is required",
      },
    ])
    .onSuccess((event) => {
      console.log('Validation passes and form submitted', event);
    })
    .onFail((fields) => {
      console.log('Form failds', fields);
    })
}

accountForm && accountValidate()