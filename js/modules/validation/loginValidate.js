import formValidate from './instance.js'

const loginForm = document.querySelector('.modal-auth__form')

export const loginValidate = () => {
  formValidate(loginForm)
    .addField('#login-phone', [
      {
        rule: 'required',
        value: true,
        errorMessage: "Phone is required",
      },
      {
        rule: 'customRegexp',
        value: /^\+38\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$/gi,
        errorMessage: 'Phone is required',
      },
    ])
    .onSuccess((event) => {
      console.log('Validation passes and form submitted', event);
    })
    .onFail((fields) => {
      console.log('Form failds', fields);
    })
}

loginForm && loginValidate()