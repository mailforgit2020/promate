import '../vendor/simplebar.min.js'

const scrollSections = document.querySelectorAll('.scroll-section')

export const setScrollSection = () => {
  if (innerWidth <= 1024 || /Mobi|Android/i.test(navigator.userAgent)) return
  
  scrollSections.forEach(section => new SimpleBar(section, { 
    autoHide: false,
  }))
}

scrollSections && scrollSections.length !== 0 && setScrollSection()