import '../../vendor/axios/axios.min.js'
import { YOUTUBE_API_KEY } from './../vars.js'

const videoEls = [...document.querySelectorAll('[data-video-id]')]
const videoTitle = document.querySelectorAll('.video-title')
const videoIds = videoEls.map(item => item.dataset.videoId)

const btn = document.querySelectorAll('.video-container--main')
const videoContainer = document.querySelectorAll('.video-container')
const videoWrapper = document.querySelectorAll('.video')

// Get video title

export function getVideoInfo(videoId) {
  return axios.get(`https://www.googleapis.com/youtube/v3/videos?part=snippet&id=${videoId}&key=${YOUTUBE_API_KEY}`)
    .then(response => response.data.items[0])
    .catch(error => {
      console.log('Помилка при отриманні даних відео:', error)
      return null
    })
}

const videoInfoArray = []

Promise.all(videoIds.map(videoId => getVideoInfo(videoId)))
  .then(videoInfos => {
    videoInfoArray.push(...videoInfos.filter(videoInfo => videoInfo !== null))
    
    videoTitle.forEach((title, index) => {
      title.textContent = videoInfoArray[index].snippet.localized.title
    })
  })

// Lazyloading for video

let tag = document.createElement('script')
tag.src = 'https://www.youtube.com/player_api'
let firstScriptTag = document.getElementsByTagName('script')[0]
videoWrapper && videoWrapper.length > 0 && firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)
let videoLink
let player

function onYouTubePlayerAPIReady(videoLink) {
  player = new YT.Player(`ytplayer-${videoLink}`, {
    height: '100%',
    width: '100%',
    videoId: videoLink,
    playerVars: {
      rel: 0,
      controls: 1,
      autoplay: 1,
      showinfo: 0,
      modestbranding: 1,
      playsinline: 1
    },
  })
}

videoContainer.forEach(v => {
  if (v.dataset.videoId  !== undefined) {
    videoLink = v.dataset.videoId
  } else {
    videoLink = v.querySelector("iframe").src.split('/').pop()
  }
})

videoContainer.forEach(v => {
  v.insertAdjacentHTML(
    'beforeend',
    '<div class="video-preview" style="background-image:url(\'http://i3.ytimg.com/vi/' +
    v.dataset.videoId +
    "/hqdefault.jpg')\"></div>"
  )
})

btn.forEach(b => {
  b.addEventListener('click', e => {
    e.preventDefault()

    b.parentElement.classList.remove('play')
    b.querySelector('iframe') && b.querySelector('iframe').remove()

    if (b.dataset.videoId  !== undefined) {
      videoLink = b.dataset.videoId
    } else {
      videoLink = b.querySelector("iframe").src.split('/').pop()
    }

    b.parentElement.classList.add('play')
    b.insertAdjacentHTML('beforeend', '<div id="ytplayer-' + videoLink + '"></div>');
    onYouTubePlayerAPIReady(videoLink);
  })
})