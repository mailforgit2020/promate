const footerAccordion = document.querySelector('.footer-lists__inner')
const productThumb = document.querySelector('.product-thumb')
const productInfo = document.querySelector('.product-info')

function initialState() {
  if (footerAccordion && innerWidth > 1025) {
    footerAccordion.querySelectorAll('.footer-list').forEach(item => item.style = '')
    footerAccordion.querySelectorAll('.footer-lists__item').forEach(item => item.classList.remove('active'))
  }

  if (productThumb && innerWidth > 1025) {
    let productInfoHeight = productInfo.offsetHeight

    productThumb.style.height = `${productInfoHeight}px`
  }
}

window.addEventListener('resize', initialState)
window.addEventListener('DOMContentLoaded', initialState)