const sectionElement = document.querySelectorAll('.checkout-contacts__body')

export const setActiveSection = () => {
  sectionElement.forEach(section => {
    for (let item of section.children) {
      item.addEventListener('click', () => {
        const activeEl = document.querySelector('.checkout-contacts__item.active')
        
        activeEl && activeEl.classList.remove('active')
        section.parentElement.classList.add('active')
      })
    }
  })
}

sectionElement.length > 0 && setActiveSection()