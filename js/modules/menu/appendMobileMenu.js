const appendMobileMenu = () => {
  const header = document.querySelector('.header')
  const mobileMenuTemplate = document.querySelector('#mobile-menu')
  let mobileMenu = document.querySelector('.mobile-menu')

  if (innerWidth <= 1024) {
    if (!mobileMenu && mobileMenuTemplate) {
      const mobileMenuClone = document.importNode(mobileMenuTemplate.content, true)
      mobileMenu = mobileMenuClone.firstElementChild
      header.insertAdjacentElement('afterend', mobileMenu)
    } else if (mobileMenu && mobileMenu.contains(mobileMenuTemplate)) {
      mobileMenu.remove()
    }
  } else {
    if (mobileMenu) {
      mobileMenu.remove()
      mobileMenu = null
    }
  }
}

window.addEventListener('DOMContentLoaded', appendMobileMenu)
window.addEventListener('resize', appendMobileMenu)