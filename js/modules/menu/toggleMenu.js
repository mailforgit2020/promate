import { disableScrollbar, enableScrollbar, fadeToggle } from '../functions.js'

const menuOpenBtn = document.querySelector('.burger')
const overlay = document.querySelector('.overlay')

export const menuOpen = () => {
  menuOpenBtn.addEventListener('click', () => {
    const menu = document.querySelector('.mobile-menu')

    menuOpenBtn.classList.add('active')
    menu.classList.add('open')

    fadeToggle(overlay, 200, 'block')

    disableScrollbar()
  })
}

export const menuClose = () => {
  document.addEventListener('click', e => {
    if (e.target.closest('.mobile-menu__times')) {
      const menu = document.querySelector('.mobile-menu')

      menuOpenBtn.classList.remove('active')
      menu.classList.remove('open')
  
      fadeToggle(overlay, 350, 'block')
  
      enableScrollbar()
    }
  })

  overlay.addEventListener('click', () => {
    const closeMenuBtn = document.querySelector('.mobile-menu__times')
    
    closeMenuBtn && closeMenuBtn.click()
  })
}

if (menuOpenBtn) {
  menuOpen()
  menuClose()
}