import '../../vendor/simplebar.min.js';

const appendDesktopMenu = () => {
  const intro = document.querySelector('.intro__inner-list')
  const desktopMenuTemplate = document.querySelector('#desktop-menu')
  let desktopMenu = document.querySelector('.intro-list')

  if (innerWidth > 1024) {
    if (!desktopMenu && desktopMenuTemplate) {
      const desktopMenuClone = document.importNode(desktopMenuTemplate.content, true)
      desktopMenu = desktopMenuClone.firstElementChild
      intro.insertAdjacentElement('afterbegin', desktopMenu)

      const lists = document.querySelectorAll('.submenu-list')

      lists.forEach(list => {
        const listInstance = new SimpleBar(list, { autoHide: false, })
        listInstance.recalculate()
      })
    } else if (desktopMenu && desktopMenu.contains(desktopMenuTemplate)) {
      desktopMenu.remove()
    }
  } else {
    if (desktopMenu) {
      desktopMenu.remove()
      desktopMenu = null
    }
  }
}

window.addEventListener('DOMContentLoaded', appendDesktopMenu)
window.addEventListener('resize', appendDesktopMenu)