const catMenuOverlay = document.querySelector('.catalog-overlay')
const header = document.querySelector('.header')
const cookie = document.querySelector('.cookie-panel')
const btnUp = document.querySelector('.btn-up')

export const toggleCatmMenu = () => {
	document.addEventListener('mousemove', e => {
		if (innerWidth <= 1024) return

		if (e.target.closest('.intro-list')) {
			catMenuOverlay.style.display = 'block'
			header.classList.add('cat-open')
			cookie && cookie.classList.add('cat-open')
			btnUp.classList.add('cat-open')
		} else {
			catMenuOverlay.style.display = ''
			header.classList.remove('cat-open')
			cookie && cookie.classList.remove('cat-open')
			btnUp.classList.remove('cat-open')
		}
	})
}


export const changeMenuPhoto = () => {
	document.addEventListener('mousemove', e => {
		if (innerWidth <= 1024) return

		if (e.target.closest('.submenu-list__item')) {
			const item = e.target.closest('.submenu-list__item')

			const parent = item.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement
		
			const img = parent.nextElementSibling.querySelector('img')
			const path = item.dataset.imgSrc
			
			img.src = path
		}
	})
}

catMenuOverlay && toggleCatmMenu()
catMenuOverlay && changeMenuPhoto()