export const openMegamenu = () => {
  document.addEventListener('click', e => {
    if (e.target.closest('.mobile-menu__arrow')) {
      const btn = e.target.closest('.mobile-menu__arrow')
      const menu = document.querySelector('.mobile-menu')

      btn.nextElementSibling.classList.add('active')
      menu.classList.add('move')
    }
  })
}

export const closeMegamenu = () => {
  document.addEventListener('click', e => {
    if (e.target.closest('.mobile-menu__back')) {
      const menu = document.querySelector('.mobile-menu')

      menu.classList.remove('move')
      setTimeout(() => document.querySelectorAll('.mobile-menu__sublist').forEach(item => item.classList.remove('active')), 350)
    }
  })
}

openMegamenu()
closeMegamenu()