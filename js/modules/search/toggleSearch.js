const header = document.querySelectorAll('.header')

export const openSearch = () => {
  document.addEventListener('submit', e => {
    if (e.target.closest('.search__form')) {
      e.preventDefault()

      const form = e.target.closest('.search__form')

      form.parentElement.classList.add('open')
    }
  })
}

export const closeSearch = () => {
  document.addEventListener('click', e => {
    if ([...document.querySelectorAll('.search__form')].some(form => form.parentElement.classList.contains('open')) && !e.target.closest('.search')) {
      document.querySelectorAll('.search__form').forEach(form => form.parentElement.classList.remove('open'))
    }
  })
}

if (header) {
  openSearch()
  closeSearch()
}
