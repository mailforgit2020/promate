const sliderLinks = document.querySelectorAll('.slider-links')
const productsSliders = document.querySelectorAll('.product-slider')
const productsSlidersSingle = document.querySelectorAll('.product ~ .product-list .product-slider, .catalog ~ .product-list .product-slider')
const blogSliders = document.querySelectorAll('.blog-slider')

export const introSlider = new Swiper(".intro-slider", {
  speed: 500,
  loop: true,
  allowTouchMove: false,
  autoplay: {
    delay: 5000,
    pauseOnMouseEnter: true,
    disableOnInteraction: false,
  },
  effect: 'fade',
  fadeEffect: {
    crossFade: true
  },
  pagination: {
    el: ".swiper-pagination",
    clickable: true
  },
})

export const introMobSlider = new Swiper('.intro-mob-slider', {
  speed: 500,
  spaceBetween: 18,
  slidesPerView: 5,
  navigation: {
    prevEl: '.intro-mob-slider__btn--prev',
    nextEl: '.intro-mob-slider__btn--next',
  },
  breakpoints: {
    768: {
      slidesPerView: 5,
    },
    586: {
      slidesPerView: 3,
    },
    0: {
      slidesPerView: 2.7,
      spaceBetween: 10,
    }
  },
  on: {
    init: swiper => {
      const sliderWrapper = swiper.el

      sliderWrapper.classList.add('begin')
      sliderWrapper.classList.remove('end')
    },
    slideChange: swiper => {
      const sliderWrapper = swiper.el
      
      if (swiper.isBeginning) {
        sliderWrapper.classList.add('begin')
        sliderWrapper.classList.remove('end')

        return
      }
      if (swiper.isEnd) {
        sliderWrapper.classList.remove('begin')
        sliderWrapper.classList.add('end')

        return
      }

      sliderWrapper.classList.remove('begin')
      sliderWrapper.classList.remove('end')
    }
  }
})

sliderLinks.forEach(slider => {
  new Swiper(slider, {
    slidesPerView: "auto",
    freeMode: true,
    freeModeSticky: true,
    mousewheel: true,
    speed: 400,
    spaceBetween: 15,
    on: {
      init: swiper => {
        const sliderWrapper = swiper.el.parentElement

        sliderWrapper.classList.add('begin')
        sliderWrapper.classList.remove('end')
      },
      slideChange: swiper => {
        const sliderWrapper = swiper.el.parentElement
        
        if (swiper.isBeginning) {
          sliderWrapper.classList.add('begin')
          sliderWrapper.classList.remove('end')

          return
        }
        if (swiper.isEnd) {
          sliderWrapper.classList.remove('begin')
          sliderWrapper.classList.add('end')

          return
        }

        sliderWrapper.classList.remove('begin')
        sliderWrapper.classList.remove('end')
      }
    }
  })
})


if (productsSlidersSingle.length > 0) {
  productsSlidersSingle.forEach(slider => {
    new Swiper(slider, {
      speed: 500,
      spaceBetween: 30,
      slidesPerView: 4,
      navigation: {
        prevEl: slider.parentElement.querySelector('.product-slider__btn--prev'),
        nextEl: slider.parentElement.querySelector('.product-slider__btn--next'),
      },
      breakpoints: {
        1367: {
          spaceBetween: 30,
          slidesPerView: 4,
        },
        1101: {
          spaceBetween: 20,
          slidesPerView: 4,
        },
        766: {
          spaceBetween: 20,
          slidesPerView: 3,
        },
        0: {
          spaceBetween: 10,
          slidesPerView: 2,
          centeredSlides: true,
          loop: true,
        }
      },
      on: {
        init: swiper => {
          const slider = swiper.el
          const slides = swiper.slides.length
          const slidesPerView = swiper.params.slidesPerView
  
          if (slides <= slidesPerView) {
            slider.parentElement.querySelector('.product-slider__btn--prev').classList.add('swiper-button-hidden')
            slider.parentElement.querySelector('.product-slider__btn--next').classList.add('swiper-button-hidden')
          }
        }
      }
    })
  })
} else {
  productsSliders.forEach(slider => {
    new Swiper(slider, {
      speed: 500,
      spaceBetween: 30,
      slidesPerView: 4,
      navigation: {
        prevEl: slider.parentElement.querySelector('.product-slider__btn--prev'),
        nextEl: slider.parentElement.querySelector('.product-slider__btn--next'),
      },
      breakpoints: {
        1367: {
          spaceBetween: 30,
          slidesPerView: 4,
        },
        1101: {
          spaceBetween: 20,
          slidesPerView: 4,
        },
        750: {
          spaceBetween: 20,
          slidesPerView: 3,
        },
        0: {
          spaceBetween: 10,
          slidesPerView: 2,
        }
      },
      on: {
        init: swiper => {
          const slider = swiper.el
          const slides = swiper.slides.length
          const slidesPerView = swiper.params.slidesPerView
  
          if (slides <= slidesPerView) {
            slider.parentElement.querySelector('.product-slider__btn--prev').classList.add('swiper-button-hidden')
            slider.parentElement.querySelector('.product-slider__btn--next').classList.add('swiper-button-hidden')
          }
        }
      }
    })
  })
}




blogSliders.forEach(slider => {
  new Swiper(slider, {
    speed: 500,
    spaceBetween: 30,
    slidesPerView: 4,
    navigation: {
      prevEl: slider.parentElement.querySelector('.blog-slider__btn--prev'),
      nextEl: slider.parentElement.querySelector('.blog-slider__btn--next'),
    },
    breakpoints: {
      1367: {
        spaceBetween: 30,
        slidesPerView: 4,
      },
      1101: {
        spaceBetween: 20,
        slidesPerView: 3,
      },
      750: {
        spaceBetween: 20,
        slidesPerView: 3,
      },
      0: {
        spaceBetween: 10,
        slidesPerView: 2,
      }
    }
  })
})

export const reviewSlider = new Swiper('.reviews-slider', {
  speed: 500,
  spaceBetween: 30,
  slidesPerView: 4,
  breakpoints: {
    1367: {
      spaceBetween: 30,
      slidesPerView: 4,
    },
    1101: {
      spaceBetween: 20,
      slidesPerView: 4,
    },
    766: {
      spaceBetween: 20,
      slidesPerView: 3,
    },
    0: {
      spaceBetween: 10,
      centeredSlides: true,
      loop: true,
      slidesPerView: 2,
    }
  }
})

export const cartSlider = new Swiper('.cart-slider', {
  speed: 500,
  spaceBetween: 30,
  slidesPerView: 2.4,
  breakpoints: {
    1025: {
      spaceBetween: 30,
      slidesPerView: 2.4,
    },
    766: {
      spaceBetween: 20,
      slidesPerView: 3,
    },
    0: {
      centeredSlides: true,
      loop: true,
      spaceBetween: 10,
      slidesPerView: 2,
    }
  }
})

export const aboutAchiveSlider = new Swiper('.about-achive-slider', {
  autoplay: {
    delay: 5000,
    pauseOnMouseEnter: false,
    disableOnInteraction: false
  },
  slidesPerView: 3,
  spaceBetween: 30,
  loop: true,
  centeredSlides: true,
  speed: 500,
  navigation: {
    prevEl: '.about-achive__slider-arrow--prev',
    nextEl: '.about-achive__slider-arrow--next'
  },
  breakpoints: {
    1367: {
      spaceBetween: 30,
      slidesPerView: 3,
    },
    1025: {
      spaceBetween: 20,
      slidesPerView: 3,
    },
    766: {
      spaceBetween: 20,
      slidesPerView: 3,
    },
    0: {
      spaceBetween: 10,
      slidesPerView: 1.8,
    }
  },
})

export const aboutTeamSlider = new Swiper('.about-team-slider', {
  autoplay: {
    delay: 5000,
    pauseOnMouseEnter: false,
    disableOnInteraction: false
  },
  slidesPerView: 4,
  spaceBetween: 30,
  loop: true,
  speed: 500,
  navigation: {
    prevEl: '.about-team__slider-arrow--prev',
    nextEl: '.about-team__slider-arrow--next'
  },
  breakpoints: {
    1367: {
      spaceBetween: 30,
      slidesPerView: 4,
    },
    1024: {
      spaceBetween: 20,
      slidesPerView: 4,
    },
    766: {
      spaceBetween: 20,
      slidesPerView: 3,
    },
    0: {
      centeredSlides: true,
      spaceBetween: 10,
      slidesPerView: 1.8,
    }
  }
})

export const aboutPartnerSlider = new Swiper('.about-partners-slider', {
  autoplay: {
    delay: 5000,
    pauseOnMouseEnter: false,
    disableOnInteraction: false
  },
  slidesPerView: 4,
  loop: true,
  speed: 500,
  navigation: {
    prevEl: '.about-partners__btn--prev',
    nextEl: '.about-partners__btn--next'
  },
  breakpoints: {
    1025: {
      slidesPerView: 4,
    },
    766: {
      slidesPerView: 4,
    },
    0: {
      centeredSlides: true,
      slidesPerView: 2,
    }
  }
})

export const aboutEmploySlider = new Swiper('.about-employ-slider', {
  autoplay: {
    delay: 5000,
    pauseOnMouseEnter: false,
    disableOnInteraction: false
  },
  slidesPerView: 8,
  speed: 500,
  spaceBetween: 3,
  navigation: {
    prevEl: '.about-employ__slider-arrow--prev',
    nextEl: '.about-employ__slider-arrow--next'
  },
  breakpoints: {
    1025: {
      slidesPerView: 8,
    },
    766: {
      slidesPerView: 5,
    },
    0: {
      centeredSlides: true,
      loop: true,
      slidesPerView: 2,
    }
  },
  on: {
    init: swiper => {
      const slider = swiper.el
      const slides = swiper.slides.length
      const slidesPerView = swiper.params.slidesPerView

      if (slides <= slidesPerView) {
        slider.parentElement.querySelector('.about-employ__slider-arrow--prev').classList.add('swiper-button-hidden')
        slider.parentElement.querySelector('.about-employ__slider-arrow--next').classList.add('swiper-button-hidden')
      }
    }
  }
})

export const videoThumbSlider = new Swiper(`${innerWidth >= 1025 ? '.product-tabs__content' : '.product-tabs__accordion'} .product-tabs__video-thumb`, {
  slideToClickedSlide: true,
  slidesPerView: 4,
  spaceBetween: 30,
  speed: 500,
  navigation: {
    prevEl: '.product-tabs__video-thumb-arrow--prev',
    nextEl: '.product-tabs__video-thumb-arrow--next',
  },
  breakpoints: {
    1367: {
      spaceBetween: 30,
      slidesPerView: 4,
    },
    1025: {
      spaceBetween: 20,
      slidesPerView: 4,
    },
    586: {
      spaceBetween: 20,
      slidesPerView: 3,
    },
    0: {
      spaceBetween: 10,
      slidesPerView: 2,
      centeredSlides: true,
      loop: true,
    }
  }
})

export const videoMainSlider = new Swiper(`${innerWidth >= 1025 ? '.product-tabs__content' : '.product-tabs__accordion'} .product-tabs__video-main`, {
  slidesPerView: 1,
  speed: 500,
  effect: 'fade',
  fadeEffect: {
    crossFade: true
  },
  thumbs: {
    swiper: videoThumbSlider
  },
  on: {
    slideChange: () => {
      const videoContainers = document.querySelectorAll('.product-tabs__video-main-inner')

      videoContainers.forEach(container => {
        container.parentElement.classList.remove('play')
        container.querySelector('.video-preview')?.nextElementSibling?.remove()
      })
    } 
  }
})

document.querySelectorAll('.tabs-body').forEach(body => {
  new Swiper(body, {
    effect: 'fade',
    autoHeight: true,
    speed: 200,
    allowTouchMove: false,
    fadeEffect: {
      crossFade: false
    },
  })
})
