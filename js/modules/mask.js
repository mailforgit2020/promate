import Inputmask from '../vendor/inputmask.es6.js'

const setMask = (selector, mask) => {
  const phoneFields = document.querySelectorAll(selector)

  const maskInstance = new Inputmask(mask, {
    showMaskOnHover: false,
    oncomplete: inp => {
      let btn = inp.target.nextElementSibling
      if (!btn) return
      btn.disabled = false
    }
  })

  phoneFields.forEach(field => {
    field.addEventListener('input', () => {
      let btn = field.nextElementSibling
      if (!btn) return
      btn.disabled = !maskInstance.isComplete()
    })
  })

  maskInstance.mask(phoneFields)

  return maskInstance
}

export default setMask

setMask('input[type="tel"]', '+38 (099) 999-99-99')