const hideBtns = document.querySelectorAll('.inp-eye')

export const hidePassword = () => {
  hideBtns.forEach(btn => {
    btn.addEventListener('click', () => {
      btn.classList.toggle('active')

      btn.previousElementSibling.type = btn.classList.contains('active') ? 'text' : 'password'
    })
  })
}

hideBtns.length > 0 && hidePassword()